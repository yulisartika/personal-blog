import React from "react";
import Post from "../Components/Post";
import { Col, Row } from "reactstrap";

export default function Blog() {
  const posts = [
    {
      title: "Meow One",
      subtitle: "Golden Meow",
      content: `Content 1 : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s`,
      image:
        "https://i.pinimg.com/originals/be/d4/6c/bed46c1177df1ccb545d66ccdb62777e.jpg",
    },

    {
      title: "Meow Two",
      subtitle: "Baby Meow",
      content:
        "Content 2: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      image:
        "https://i.pinimg.com/474x/0a/6a/ec/0a6aecbd7b4049dc63c68c8a097e1907.jpg",
    },
    {
      title: "Meow Three",
      subtitle: "Cheeky Meow",
      content:
        "Content 3: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      image:
        "https://i.pinimg.com/originals/bc/c7/60/bcc760d8aa1f4b81b6fc0824b69cb927.jpg",
    },
    {
      title: "Meow Four",
      subtitle: "Cheeky Meow",
      content:
        "Content 4: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      image:
        "https://pbs.twimg.com/profile_images/1089487574492680192/gjaa3FKV_400x400.jpg",
    },
  ];

  const postComponents = posts.map((post) => (
    <Col md="3">
      <Post
        title={post.title}
        subtitle={post.subtitle}
        content={post.content}
        image={post.image}
      />
    </Col>
  ));

  return (
    <div>
      <h1 className="blog-header">BLOG PAGE</h1>
      <Row>{postComponents}</Row>
    </div>
  );
}
