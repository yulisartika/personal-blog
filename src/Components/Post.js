import {
  Card,
  Button,
  CardImg,
  CardTitle,
  CardText,
  CardSubtitle,
  CardBody,
} from "reactstrap";

import React from "react";

export default function Post({ title, content, image, subtitle }) {
  const handleClick = (event) => {
    event.preventDefault();
    alert(`You just clicked! ${title}`);
  };

  return (
    <div>
      <Card>
        <CardImg top width="100%" src={image} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">{title}</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">
            {subtitle}
          </CardSubtitle>
          <CardText>{content}</CardText>

          <Button type="submit" onClick={handleClick}>
            Button
          </Button>
        </CardBody>
      </Card>
    </div>
  );
}
